;; init.el --- Personal GNU Emacs configuration file.

;; Copyright (c) 2019 Shripad Sinari <shripad.sinari@gmail.com>

;; This file is not part of GNU Emacs.  Its source online is:
;; https://https://bitbucket.org/ssinari/dotemacs

;;; License:

;; This file is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.
;;
;; This file is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file sets up the essentials for incorporating my init org
;; file.  It uses straight.el and use-package. Straight is being used as a
;; package manager and use-package for loading, dependency management, config,
;; etc. Most of the code in this file is to load
;; the ssinari-init.org which is a literate config file. For more on literate
;; programming see https://en.wikipedia.org/wiki/Literate_programming.

;;; Code:

(setq user-emacs-directory "~/.emacs.d/")

(require 'org)
(org-babel-load-file (expand-file-name "~/.emacs.d/ssinari-public-init.org"))

;;; init.el ends here
